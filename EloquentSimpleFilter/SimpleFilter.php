<?php

namespace App\Models;

trait SimpleFilter
{
    /**
     * Redefine the Illuminate\Database\Eloquent\Builder
     *
     * @param $query
     * @return CustomEloquentBuilder
     */
    public function newEloquentBuilder($query)
    {
        return new CustomEloquentBuilder($query);
    }
}